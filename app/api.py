from app.models import Game
from app.extensions import db
from app.serializers import game_schema
from flask import request, jsonify


def get_version():
    return {"version": 1.0}


# Create a Game
def create_game():
  new_Game = Game()
  db.session.add(new_Game)
  db.session.commit()
  return game_schema.dump(new_Game)

# get a game from id
def get_game(id):
    game = Game.query.get(id)
    return game_schema.dumps(game)

# player  place a mark
def place_mark(game_id):

    game = Game.query.get(game_id)
    mark_value = request.json['player']
    mark_cord = [request.json['row'], request.json['col']]

    # coordinates converts to list index
    mark_list_index = int(mark_cord[0])*3 + int(mark_cord[1])
    grid = list(game.grid)

    # is already filled ?
    if grid[mark_list_index] == '.':
      grid[mark_list_index] = mark_value
    else:
      return jsonify({"error": "This place is already filled."})

    # is over  and winner
    result = is_over_winner(grid)

    game.grid = ''.join(grid)
    game.is_winner = result["winner"]
    game.is_over = result["status"]
    db.session.commit()
    return game_schema.dump(game)

# ###########
# game logic
# ###########


def is_over_winner(grid: list) -> dict:
    """ Find  if the game is over and who won

    Args:
        grid (list): grid of the game with the maks

    Returns:
        dict:  "status":  if the game is over
                "winner":  winner for the game
    """

    # not possible to finish the game before 5 moves
    if grid.count(".") > 4:
        return {"status": False, "winner": "?"}

    # check if player has won 
    # a cross at the  top

    if grid.count(".") <= 5:
        if grid[6] == grid[7] == grid[8] != '.':
            return {"status": True, "winner": grid[6]}

        # across the middle
        elif grid[3] == grid[4] == grid[5] != '.':
            return {"status": True, "winner": grid[3]}

        # across the bottom
        elif grid[0] == grid[1] == grid[2] != '.':
            return {"status": True, "winner": grid[0]}

        # down the left side
        elif grid[0] == grid[3] == grid[6] != '.':
            return {"status": True, "winner": grid[0]}

        # down the middle
        elif grid[1] == grid[4] == grid[7] != ' ':
            return {"status": True, "winner": grid[1]}

        # down the right side
        elif grid[2] == grid[5] == grid[8] != ' ':
            return {"status": True, "winner": grid[2]}

        # diagonal
        elif grid[6] == grid[4] == grid[2] != ' ':
            return {"status": True, "winner": grid[6]}

        # diagonal
        elif grid[0] == grid[4] == grid[8] != '.':
            return {"status": True, "winner": grid[0]}

        #  grid is full, we'll declare the result as 'tie'.
    if grid.count(".") == 0:
        return {"status": True, "winner": "tie"}

######################## 
# Testing to not Implemented but prepared

# #not over
# assert is_over_winner(["X","0","X",
#                 "0",".",".",
#                 ".",".","."]) == {'status': False, 'winner': '?'}
# # across the top
# assert is_over_winner([".",".",".",
#                 "0","0",".",
#                 "X","X","X"]) == {'status': 'Game Over', 'winner': 'X'}

# # across the bottom
# assert is_over_winner(["X","X","X",
#                 "0","0",".",
#                 "0","0","X"]) == {"status" : True, "winner": "X" }
# # down the left side
# assert is_over_winner(["X","X","0",
#                 "X","0",".",
#                 "X","0","X"]) == {"status" : True, "winner": "X" }
# diagonale