import connexion
from app import api as API
from app import extensions


def create_app():
    # Setup connexion
    connexion_app = connexion.FlaskApp(__name__)
    connexion_app.add_api('api.yaml')
    flask_app = connexion_app.app

    # Flask setup
    extensions.init_app(flask_app)

    @flask_app.route('/')
    def get_version():
        return API.get_version()

    @flask_app.route('/game', methods=['POST'])
    def create_game():
         return API.create_game()

    @flask_app.route('/game/<id>', methods=['GET'])
    def get_game(id):
         return API.get_game(id)

    @flask_app.route('/game/<id>', methods=['PATCH'])
    def place_mark(id):
         return API.place_mark(id)

    return flask_app
