from app.extensions import db
import enum

# class Players(enum.Enum):
#     X = "X"
#     O = "0"


class Game(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  grid = db.Column(db.String(9),)
  # current_player = db.Column(db.Enum(Players))
  current_player = db.Column(db.String(1))
  is_over = db.Column(db.Boolean())
  is_winner = db.Column(db.String(1),)

  def __init__(self, ):

    self.grid = "........."
    self.current_player = "X"
    self.is_over = False
    self.is_winner = "?"
