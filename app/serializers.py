# Game Schema
from marshmallow import Schema
from app.models import Game


class GameSchema(Schema):
  model = Game

  class Meta:
    fields = ('id', "grid", "current_player", "is_winner", "is_over")


# Init schema
game_schema = GameSchema()
# games_schema = GameSchema(many=True)
