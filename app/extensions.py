from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import os
db = SQLAlchemy()
ma = Marshmallow()


def init_app(app):

    basedir = os.path.abspath(os.path.dirname(__file__))
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir,
                'GAMES.db')
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)
    ma = Marshmallow(app)
    return (app, db, ma)
